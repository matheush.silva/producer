package com.example.producer;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.jms.support.destination.JndiDestinationResolver;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.jndi.JndiTemplate;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.naming.Context;
import java.util.Properties;

@Configuration
public class ConfigurationShit {

    @Bean
    @Primary
    public JndiObjectFactoryBean queueConnectionFactory() {
        JndiObjectFactoryBean queueConnectionFactory = new JndiObjectFactoryBean();
        queueConnectionFactory.setJndiTemplate(jndiTemplate());
        queueConnectionFactory.setJndiName("jms/BFG_XAQueueConnectionFactory");
        return queueConnectionFactory;
    }

    @Bean
    public JndiTemplate jndiTemplate() {
        JndiTemplate penis = new JndiTemplate();
        Properties properties = new Properties();
        properties.setProperty(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
        properties.setProperty(Context.PROVIDER_URL, "t3://10.29.94.65:61002");
        penis.setEnvironment(properties);
        return penis;
    }

    @Bean
    public JndiDestinationResolver jmsDestinationResolver() {
        JndiDestinationResolver destResolver = new JndiDestinationResolver();
        destResolver.setJndiTemplate(jndiTemplate());
        destResolver.setCache(true);
        return destResolver;
    }

    @Bean
    public JmsTemplate queueSenderTemplate() {
        JmsTemplate template = new JmsTemplate();
        template.setConnectionFactory((ConnectionFactory) queueConnectionFactory().getObject());
        template.setDestinationResolver(jmsDestinationResolver());
        return template;
    }

    @Bean
    public DefaultJmsListenerContainerFactory messageListener() {

        DefaultJmsListenerContainerFactory factory =
                new DefaultJmsListenerContainerFactory();

        factory.setDestinationResolver(jmsDestinationResolver());
        factory.setConnectionFactory((ConnectionFactory) queueConnectionFactory().getObject());
        /*DefaultMessageListenerContainer listener = new DefaultMessageListenerContainer();
        listener.setConcurrentConsumers(5);
        listener.setConnectionFactory((ConnectionFactory) queueConnectionFactory().getObject());
        listener.setDestination((Destination) jmsQueue().getObject());
        listener.setMessageListener(queueListener());*/

        return factory;
    }
}
