package com.example.producer;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.jms.Destination;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ProducerService {
    @Autowired private JmsTemplate jmsTemplate;

    @Scheduled(fixedDelay = 5000)
    public void produce() throws IOException {

        log.info("Reading Data to Send");
        final ObjectMapper mapper = new ObjectMapper();

        Files.list(Paths.get("./data"))
                .map(aPath -> parseFile(mapper, aPath))
                .filter(Objects::nonNull)
                .peek(System.out::println)
                .forEach(line -> jmsTemplate.convertAndSend("jms/MPLN_VE_INBOUND_QUEUE", line/*, message -> {
                    message.setJMSCorrelationID("e8143de0-ecfe-436c-8219-2c0752166aa0");
                    //                    message.setJMSReplyTo(new ActiveMQQueue("inbound.queue"));
                    return message;
                }*/));

        Files.list(Paths.get("./data"))
                .forEach(path -> path.normalize().toFile().delete());


    }

    private String parseFile(ObjectMapper mapper, Path aPath) {

        try {
            return Files.readAllLines(aPath, StandardCharsets.UTF_8).stream().collect(Collectors.joining());
        } catch (IOException e) {
            e.printStackTrace();

        }
        /*try {
            return mapper.readValue(aPath.normalize().toFile(), String.class);
        } catch (IOException e) {
            log.error("Error reading file ", e);
            return null;
        }*/
        return null;
    }
}
