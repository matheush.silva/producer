package com.example.producer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ConsumerService {


    @JmsListener(destination = "jms/MPLN_VE_OUTBOUND_QUEUE")
    public void meuPau(final Message jsonMessage) {
        log.info("AEE RECEBI: {}", jsonMessage.getPayload());
    }

}
